import { reduce } from "../reduce.js"

const items = [1, 2, 3, 4, 5, 5];

try {
    const result = reduce(items,(acc,num) => {
        return acc + num
    },0)
    console.log(result)

} catch (error) {
    console.log(error.message)
}
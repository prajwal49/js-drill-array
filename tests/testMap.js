import { map } from "../map.js"

const items = [1, 2, 3, 4, 5, 5];

try {
    const result = map(items,elements => {
        return elements;
    })
    console.log(result)
    
    const double = map(items,elements => {
        return elements * 2;
    })

    console.log(double)
} catch (error) {
    console.log(error.message)
}
import { find } from "../find.js";

const items = [1, 2, 3, 4, 5, 5];

try {
  const result = find(items, (elements) => {
    return elements > 3;
  });
} catch (error) {
  console.log(error.message);
}

console.log(result);

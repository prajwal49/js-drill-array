import { each } from "../each.js"

const items = [1, 2, 3, 4, 5, 5];

try {
    each(items, element => {
        console.log(`Element: ${element}`);
    });
    
} catch (error) {
    console.log(error.message)
}

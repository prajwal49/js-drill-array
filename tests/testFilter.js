import { filter } from "../filter.js";

const items = [1, 2, 3, 4, 5, 5];

try {
  let result = filter(items, (element) => {
    return element > 3;
  });

  console.log(result);
} catch (error) {
  console.log(error.message);
}

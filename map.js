export function map(elements,cb){

    if(!Array.isArray(elements)){
        throw new Error("Elements should be an array");
    }
    
    if(typeof cb !== 'function'){
        throw new Error("second argument should be function")
    }
    let array = []
    for(let ele = 0;ele<elements.length;ele++){
        array.push(cb(elements[ele]))
    }

    return array
}
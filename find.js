export function find(elements,cb){
    
    if(!Array.isArray(elements)){
        throw new Error("Elements should be an array");
    }

    for(let i = 0;i<elements.length;i++){
        if(cb(elements[i])){
            return elements[i];
        }
        
    }
    return undefined;
}
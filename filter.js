export function filter(element, cb) {
  if (!Array.isArray(element)) {
    throw new Error("Elements should be an array");
  }
  let filteredElement = [];
  for (let i = 0; i < element.length; i++) {
    if (cb(element[i])) {
      filteredElement.push(element[i]);
    }
  }
  return filteredElement;
}

export function reduce(elements,cb,startingValue){

    if(!Array.isArray(elements)){
        throw new Error("Elements should be an array");
    }
    
    let accumulator = startingValue;
    let startIndx = 0;

    if(startingValue === 'undefined'){
        accumulator = elements[0];
        startIndx = 1;
    }
    for(let ele = startIndx;ele < elements.length;ele++){
        accumulator = cb(accumulator,elements[ele]);
    }
    return accumulator;
}
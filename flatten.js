export function flatten(element) {
  if (!Array.isArray(element)) {
    throw new Error("Elements should be an array");
  }
  let flattenElements = [];

  function flatten_helper(arr) {
    for (let i = 0; i < arr.length; i++) {
      if (Array.isArray(arr[i])) {
        flatten_helper(arr[i]);
      } else {
        flattenElements.push(arr[i]);
      }
    }
  }

  flatten_helper(element);

  return flattenElements;
}
